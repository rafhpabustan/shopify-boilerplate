# Gulp + Shopify

## Installation

### Gulp.js

Clone the repo into your project root.

In Terminal `cd` into the `/dev` directory and install the Gulp packages (if you haven’t already installed Gulp, you’ll need to [do so](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md "Gulp installation") first):

`npm install`

Once you have installed the packages, in Terminal, run `gulp watch`.

Any changes to the SCSS files in `/dev/sass/` will lead to the creation of `theme.scss.liquid` in `assets`.

Any alterations to the JS files in `/dev/js/functions/` will be concatenated and uglified in `/assets` to `theme.js` and `theme.min.js`.

Images added to `/dev/image` will be copied across to the `/assets` directory. Similarly, any fonts added to `/dev/font` will be copied across to `/assets`.

### Theme Watch + Gulp Watch (Recommended)

To get Theme Kit up and running, follow the instructions [here](https://shopify.github.io/themekit/#installation "Theme Kit installation instructions").

Provide necessary details on `dev/config.js` and bundle assets with `gulp` or watch with `gulp watch`
Note: Details on config requires to create a private app on shopify: [Authenticate a Private App](https://shopify.dev/tutorials/authenticate-a-private-app-with-shopify-admin#:~:text=From%20your%20Shopify%20admin%2C%20go,and%20a%20contact%20email%20address).

### Theme Kit (Unrecommended / Advanced Usage)

The `config.yml` file is included in the repo, ready to be filled in with the necessary details.

To get started on your theme, follow [these instructions](https://shopify.github.io/themekit/#use-a-new-theme "Theme Kit usage instructions").

## Features

The Gulp build features the following helpful packages:

* [gulp-autoprefixer](https://github.com/sindresorhus/gulp-autoprefixer "gulp-autoprefixer Github page")
* [gulp-babel](https://github.com/babel/gulp-babel "gulp-babel Github page")
* [gulp-clean-css](https://github.com/scniro/gulp-clean-css "gulp-clean-css Github page")
* [gulp-concat](https://github.com/contra/gulp-concat "gulp-concat Github page")
* [gulp-csslint](https://github.com/lazd/gulp-csslint "gulp-csslint Github page")
* [gulp-rename](https://github.com/hparra/gulp-rename "gulp-rename Github page")
* [gulp-sass](https://github.com/dlmanning/gulp-sass "gulp-sass Github page")
* [gulp-uglify](https://github.com/terinjokes/gulp-uglify "gulp-uglify Github page")
* [gulp-scss-lint](https://github.com/juanfran/gulp-scss-lint "gulp-scss-lint Github page")

## Credits

* [Shopify Skeleton Theme](https://github.com/Shopify/skeleton-theme "Shopify Skeleton Theme Github page")
* [Gulp.js](http://gulpjs.com/ "Gulp.js website")
* [Theme Kit](https://shopify.github.io/themekit/ "Shopify Theme Kit Github page")
* [SASS / SCSS](http://sass-lang.com/ "SASS website")
