'use strict'

const gulp = require('gulp')
const babel = require('gulp-babel')
const autoprefixer = require('gulp-autoprefixer')
const changed = require('gulp-changed')
const concat = require('gulp-concat')
const rename = require("gulp-rename")
const sass = require('gulp-sass')
const uglify = require('gulp-uglify')
const gulpShopify = require('gulp-shopify-upload')
const watch = require('gulp-watch')
const config = require('./config.json')

// Asset paths
const scssSrc = 'scss/**/*.scss'
const jsSrc = 'js/**/*.js'
const assetsDir = '../assets/'
const imageSrc = 'image/**'
const imageFiles = 'image/**/*.{jpg,jpeg,png,gif,svg}'
const fontSrc = 'font/**'
const fontFiles = 'font/**/*.{eot,svg,ttf,woff,woff2}'
const shopifyFiles = '../+(assets|layout|config|snippets|templates|locales|sections)/**'

// Theme task
const options = { basePath: '../'  }
const {
  shopify_api_key,
  shopify_api_password,
  shopify_url,
  theme_id
} = config

gulp.task('theme-watch', () =>
  watch(shopifyFiles)
  .pipe(gulpShopify(
    shopify_api_key,
    shopify_api_password,
    shopify_url,
    theme_id,
    options))
)

// SCSS task
gulp.task('css', () =>
  gulp.src(scssSrc)
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer({
    cascade: false
  }))
  .pipe(rename('theme.scss'))
  .pipe(gulp.dest(assetsDir))
)

// JS task
gulp.task('js', () =>
  gulp.src(jsSrc)
  .pipe(babel({
    presets: ['@babel/preset-env']
  }))
  .pipe(concat('theme.js'))
  .pipe(gulp.dest(assetsDir))
  .pipe(rename('theme.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest(assetsDir))
)

// Images task
gulp.task('images', () =>
  gulp.src(imageSrc)
  .pipe(changed(assetsDir))
  .pipe(gulp.dest(assetsDir))
)

// Fonts task
gulp.task('fonts', () =>
  gulp.src(fontSrc)
  .pipe(changed(assetsDir))
  .pipe(gulp.dest(assetsDir))
)

// Watch task
gulp.task('watch-assets', () => {
  gulp.watch(scssSrc, ['css'])
  gulp.watch(jsSrc, ['js'])
  gulp.watch(imageFiles, ['images'])
  gulp.watch(fontFiles, ['fonts'])
})

// Commands
gulp.task('default', ['css', 'js', 'images', 'fonts']) // Default "gulp"
gulp.task('watch', ['watch-assets', 'theme-watch'])
